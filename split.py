from os import listdir, path
from hashlib import md5
from zlib import compress

inpath = "C:\\Users\\user\\Downloads\\WindowsLGFirmwareExtract-1.2.5.0-Release\\system\\"
imgpath = "C:\\Users\\user\\Downloads\\Inject_Root_G4\\rootedsystem.img"
outpath = "C:\\Users\\user\\Downloads\\Inject_Root_G4\\"

filenames = listdir(inpath)
ofsts = sorted(map(lambda name: int(name.split("_")[1].split(".")[0]),
                   filenames))
sizes = map(lambda ofst: path.getsize(inpath + "system_" + str(ofst) + ".bin"),
            ofsts)
stats = zip (ofsts, sizes)

with open(imgpath, "rb") as img:
    for (ofst, size) in stats:
        with open(outpath + "system_" + str(ofst) + ".bin.z", "wb") as out:
            data = img.read(size)
            print(ofst, md5(data).hexdigest())
            out.write(compress(data))
