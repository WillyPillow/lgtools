from os import listdir, path
from functools import partial
from struct import pack
from math import ceil
import re

binPath = './bin/'
header = b'\x44\xDD\x55\xAA\xFF\xFF\xFF\xFF'
model = b'\x34\x12\x78\x56LG-H962'
fname = b'H96220a'
binRegexStr = """(^.+)_(\d+)\.bin$"""
binRegex = re.compile(binRegexStr)

bins = listdir(binPath)
ofsts = map(lambda name: int(binRegex.match(name).group(2)), bins)
parts = sorted(zip(bins,ofsts), key = lambda e: e[1])

with open('out.tot', 'wb') as tot:
    tot.write(header)
    tot.write(b'\xFF' * (8192-tot.tell()) +
              b'\x33\xEC\x55\xAA' + b'\xFF' * 12)
    ofst = 0
    for i, e in enumerate(parts):
        diskOfst = e[1]
        length = ceil(path.getsize(binPath + e[0]) / 512.0)
        unknown = 0
        tot.write(pack('<4I', diskOfst, ofst, length, unknown))
        ofst += length
    tot.write(b'\xFF' * (24592-tot.tell()) +
              model +
              b'\0' * (20-len(model)) +
              fname +
              b'\0' * (194-len(fname)) +
              b'user')
    tot.write(b'\0' * (24821-tot.tell()))
    tot.write(b'\xFF' * (25136-tot.tell()))
    for e in parts:
        partName = binRegex.match(e[0]).group(1).encode('ASCII')
        tot.write(partName + b'\0' * (32-len(partName)))
    tot.write(b'\xFF' *  (1048576-tot.tell()))
    for e in parts:
        print(e)
        with open(binPath + e[0], 'rb') as bin:
            for chunk in iter(partial(bin.read, 512), b''):
                tot.write(chunk)
    
